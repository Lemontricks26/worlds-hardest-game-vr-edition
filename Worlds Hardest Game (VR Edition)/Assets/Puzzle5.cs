﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle5 : MonoBehaviour {

    private Vector3 start;
    private bool checkpoint1 = false;
    private bool checkpoint2 = false;

    void Start () {
        start = transform.position;
	}
	
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Ball")
        {
            if(checkpoint1 == false && checkpoint2 == false)
            {
                transform.position = start;
            }
            else if(checkpoint1 == true)
            {
                transform.position = new Vector3(-7.5f, -0.2f, 9.5f);
            }
            else if(checkpoint2 == true)
            {
                transform.position = new Vector3(-5.5f, -0.2f, -6.5f);
            }
        }
        if(col.gameObject.tag == "Point2")
        {
            checkpoint1 = true;
            Destroy(col.gameObject);
        }
        if (col.gameObject.tag == "Point3")
        {
            col.gameObject.GetComponent<Collider>().isTrigger = true;
            checkpoint2 = true;
            checkpoint1 = false;
        }
    }
}
