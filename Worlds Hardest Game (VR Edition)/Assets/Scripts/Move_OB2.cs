﻿using UnityEngine;
using System.Collections;

public class Move_OB2 : MonoBehaviour
{
    public int num;
    public float speed;

    void Update()
    {
        switch (num)
        {
            case 1:
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(-4.5f, transform.position.y, transform.position.z), speed * Time.deltaTime);
                if (transform.position.x == -4.5f)
                {
                    num = 2;
                }
                break;
            case 2:
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(0.5f, transform.position.y, transform.position.z), speed * Time.deltaTime);
                if (transform.position.x == 0.5f)
                {
                    num = 1;
                }
                break;
        }
    }
}