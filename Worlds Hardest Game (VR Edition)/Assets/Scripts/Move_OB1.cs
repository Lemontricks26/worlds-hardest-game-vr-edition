﻿using UnityEngine;
using System.Collections;

public class Move_OB1 : MonoBehaviour {

    public int num;
    public float speed;
	
	void Update () {
	    switch(num)
        {
            case 1:
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, 4), speed * Time.deltaTime);
                if(transform.position.z == 4)
                {
                    num = 2;
                }
                break;
            case 2:
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, -5), speed * Time.deltaTime);
                if (transform.position.z == -5)
                {
                    num = 1;
                }
                break;
        }
	}
}
