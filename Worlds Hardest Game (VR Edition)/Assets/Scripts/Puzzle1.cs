﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Puzzle1 : MonoBehaviour {

    private Vector3 start;

	void Start()
    {
        start = transform.position;
	}
	
	void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Ball")
        {
            transform.position = start;
            // Add 1 to death
        }
        if (col.gameObject.tag == "Point2")
        {
            SceneManager.LoadScene("Puzzle 2");
        }

        if (col.gameObject.tag == "Wall")
        {
            this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}