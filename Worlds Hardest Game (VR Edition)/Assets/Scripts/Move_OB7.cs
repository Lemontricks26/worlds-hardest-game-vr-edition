﻿using UnityEngine;
using System.Collections;

public class Move_OB7 : MonoBehaviour {

    public int num;
    public float speed;

    void Update()
    {
        switch (num)
        {
            case 1:
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, 20), speed * Time.deltaTime);
                if (transform.position.z == 20)
                {
                    num = 2;
                }
                break;
            case 2:
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, 13), speed * Time.deltaTime);
                if (transform.position.z == 13)
                {
                    num = 1;
                }
                break;
        }
    }
}
