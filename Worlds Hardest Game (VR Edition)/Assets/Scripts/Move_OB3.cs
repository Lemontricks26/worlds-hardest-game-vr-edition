﻿using UnityEngine;
using System.Collections;

public class Move_OB3 : MonoBehaviour {

    public int num;
    public float speed;

	void Start () {
	
	}
	
	void Update () {
	    switch(num)
        {
            case 1:
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(-2, transform.position.y, 1), speed * Time.deltaTime);
                if (transform.position.x == -2 && transform.position.z == 1)
                {
                    num = 2;
                }
                break;
            case 2:
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(1, transform.position.y, 1), speed * Time.deltaTime);
                if (transform.position.x == 1 && transform.position.z == 1)
                {
                    num = 3;
                }
                break;
            case 3:
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(1, transform.position.y, -2), speed * Time.deltaTime);
                if (transform.position.x == 1 && transform.position.z == -2)
                {
                    num = 4;
                }
                break;
            case 4:
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(-2, transform.position.y, -2), speed * Time.deltaTime);
                if (transform.position.x == -2 && transform.position.z == -2)
                {
                    num = 1;
                }
                break;
        }
	}
}