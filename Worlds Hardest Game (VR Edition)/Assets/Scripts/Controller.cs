﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {
    
    [SerializeField]
    Transform vrHead;

    private float y;
    
	void Start () {
        y = transform.position.y;
	}
	
	void Update () {

        Vector3 forward = vrHead.TransformDirection(Vector3.forward);
        Vector3 right = vrHead.TransformDirection(Vector3.right);

        transform.position = transform.position + forward * Input.GetAxis("Vertical") * Time.deltaTime * 2;
        transform.position = transform.position + right * Input.GetAxis("Horizontal") * Time.deltaTime * 2;
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, y, y), transform.position.z);
    }
}
