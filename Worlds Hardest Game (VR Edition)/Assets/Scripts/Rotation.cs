﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

    public float finalSpeed = 1;
    public float speed = 0.5f;
    private Quaternion rotation;

	void Start () {
        
	}
	
	void Update () {
        finalSpeed += speed;
        rotation = Quaternion.Euler(0, finalSpeed, 0);
        transform.rotation = rotation;
	}
}
