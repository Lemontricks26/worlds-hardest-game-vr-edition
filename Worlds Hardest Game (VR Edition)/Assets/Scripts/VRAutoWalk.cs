﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class VRAutoWalk : MonoBehaviour {

    public float speed = 3.0f;
    public bool moveForward;
    CharacterController controller;
    //GvrViewer gvrViewer;
    [SerializeField] Transform vrHead;

	void Start () {
        controller = GetComponent<CharacterController>();
        //gvrViewer = transform.GetChild(0).GetComponent<GvrViewer>();
        //vrHead = Camera.main.transform;
	}
	
	public void Update ()
    {
        if (Input.GetButtonDown("Tap"))
        {
            moveForward = !moveForward;
        }

        if(moveForward)
        {
            Vector3 forward = vrHead.TransformDirection(Vector3.forward);
            controller.SimpleMove(forward * speed);
        }
	}
}
