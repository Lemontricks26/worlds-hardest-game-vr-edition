﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Puzzle3 : MonoBehaviour {

    private Vector3 start;
    private bool collected;
    public GameObject yellow;

	void Start()
    {
        collected = false;
        start = transform.position;
	}

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Ball")
        {
            transform.position = start;
            yellow.SetActive(true);
            collected = false;
        }
        if(col.gameObject.tag == "Point1" && collected == true)
        {
            SceneManager.LoadScene("Puzzle 4");
        }
        if(col.gameObject.tag == "Yellow")
        {
            col.gameObject.SetActive(false);
            collected = true;
        }
    }
}