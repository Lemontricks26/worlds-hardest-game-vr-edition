﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Puzzle4 : MonoBehaviour {

    private Vector3 start;
    public GameObject[] Yellow;
    private int num = 0;

	void Start () {
        start = transform.position;
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Ball")
        {
            transform.position = start;
            for(int i = 0; i < 3; i++)
            {
                Yellow[i].SetActive(true);
            }
            num = 0;
            // Add 1 to death
        }
        if (col.gameObject.tag == "Point2" && num == 3)
        {
            SceneManager.LoadScene("Puzzle 5");
        }
        if (col.gameObject.tag == "Yellow")
        {
            col.gameObject.SetActive(false);
            ++num;
        }
    }
}